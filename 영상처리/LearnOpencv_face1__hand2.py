#setup command pip install pyautogui===0.9.39

import cv2, dlib, sys
import numpy as np
import random as r
import pyautogui as py

def nothing(x):
    pass
def WindowResize(src,x,y):              #윈도우 창 크기 조정
    return cv2.resize(src,(x,y))
def DefaultWindowResize(src):
    return cv2.flip(cv2.resize(src,(400,300)),1) #화면 좌우 반전 + 크기조정
x,y = py.size()
print("ScreenSize : ",x)
##cap = cv2.VideoCapture('Hand.mp4')
#print('width: {0}, height: {1}'.format(cap.get(3),cap.get(4)))
Black = np.zeros((x,y),dtype=np.uint8)
cap = cv2.VideoCapture(0)


while(True):
    try:
        ret, hand = cap.read()
        hand_copy = hand.copy()
        cv2.imshow("Default Video",DefaultWindowResize(hand))  #영상의 원본
        hand = cv2.resize(hand,dsize=(x,y),interpolation=cv2.INTER_AREA)  #영상의 사이즈를 화면의 크기로 고정
        HSV = cv2.cvtColor(hand, cv2.COLOR_BGR2HSV)  #BGR to HSV

        # Defining HSV Threadholds
        hand_lower = np.array([0, 48, 80])
        hand_upper = np.array([20, 255, 255])   #skin color
        mask = cv2.inRange(HSV, hand_lower, hand_upper)
        
        cv2.imshow("maskVideo",DefaultWindowResize(mask))
        
        ret, mask = cv2.threshold(mask, 127, 255,0)  # Region lying in HSV range of hand_lower and hand_upper has intensity : 255, rest 0
        kernel = np.ones((7, 7), np.uint8)
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN,kernel)  # Performing Open operation (Increasing the white portion) to remove the noise from image

        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE,kernel)  # Performing the Close operation (Decreasing the white portion)

        mask = cv2.bilateralFilter(mask, 5, 65,65)  # Applying bilateral filter to further remove noises in image and keeping the boundary of hands sharp.
        cv2.imshow("Filter",DefaultWindowResize(mask))


        contour,_ = cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        drawing = np.zeros(hand.shape,np.uint8)
        max = 0
        ci = 0
        for i in range(len(contour)):
            cnt = contour[i]
            area = cv2.contourArea(cnt)
            if area > max:
                max = area
                ci = i
        cnt = contour[ci]
        epsilon = 0.25 * cv2.arcLength(cnt,True)  # Further trying to better approximate the contour by making edges sharper and using lesser number of points to approximate contour cnt.
        approx = cv2.approxPolyDP(cnt, epsilon, True)
        hull = cv2.convexHull(cnt, returnPoints=True)  # Finding the convex hull of largest contour
        cv2.drawContours(hand, [cnt], 0, (255, 0, 0), 3)  # storing the hull points and contours in "frame" image variable(matrix).
        cv2.drawContours(hand, [hull], 0, (0, 255, 0), 3)  # (")
        hull = cv2.convexHull(cnt, returnPoints=False)
        defects = cv2.convexityDefects(cnt, hull)


        count = 0  #손가락 개수 찾기
        for i in range(defects.shape[0]):
            s, e, f, d = defects[i, 0]
            start = tuple(cnt[s][0])
            end = tuple(cnt[e][0])
            far = tuple(cnt[f][0])

##            print(d)
##            print("###########")

            if d > 40000 and d < 80000:
                cv2.line(hand, start, end, [0, 255, 0], 5)
                cv2.circle(hand, far, 5, [0, 0, 255], -1)
                cv2.circle(hand, far, 5, [0, 0, 255], -1)  # draw a circle/ dot at the defect point.
                count += 1  # count is keeping track of number of defect points

        font = cv2.FONT_HERSHEY_COMPLEX
        cv2.putText(hand, str(count + 1), (100, 100), font, 1, (0, 0, 255),1)  # Outputting "count + 1"in "frame"and displaying the output.


        mmt = cv2.moments(cnt)

        cx = int(mmt['m10'] / mmt['m00'])
        cy = int(mmt['m01'] / mmt['m00'])
        #print("(", cx, " , ", cy, ")")
        midPoint = cv2.circle(hand, (int(cx), int(cy)), 1, (0, 0, 255), 10) #중앙점
        cv2.imshow("hand", DefaultWindowResize(hand))
##        x, y = py.size()
##        print(py.size()) #화면 사이즈
##        py.moveTo(x-int(cx),int(cy))
##        print(py.position())


    except:
        print("ee")



    if cv2.waitKey(1) & 0xff == ord('q'):  # q버튼을 누르면 영상 꺼짐
        break









cap.release()
cv2.destroyAllWindows()

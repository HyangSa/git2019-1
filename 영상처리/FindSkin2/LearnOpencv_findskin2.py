import cv2
import numpy as np
import random as r

from scipy.spatial import ConvexHull
def nothing(x):
    pass
def WindowResize(src,x,y):              #윈도우 창 크기 조정
    return cv2.resize(src,(x,y))
def DefaultWindowResize(src):
    return cv2.flip(cv2.resize(src,(300,300)),1) #화면 좌우 반전 + 크기조정

cap = cv2.VideoCapture('Hand.mp4')
#print('width: {0}, height: {1}'.format(cap.get(3),cap.get(4)))
#Black = np.zeros((960,540),dtype=np.uint8)
#cap = cv2.VideoCapture(0)

cv2.namedWindow('img_result')
while(True):

    ret, hand = cap.read()
    HSV = cv2.cvtColor(hand, cv2.COLOR_BGR2HSV)




    # Defining HSV Threadholds
    lower_threshold = np.array([0, 48, 80], dtype=np.uint8)  # [0, 48, 80]
    upper_threshold = np.array([20, 255, 255], dtype=np.uint8)

    skinMask = cv2.inRange(HSV, lower_threshold, upper_threshold)

    # Cleaning up mask using Gaussian Filter
    skinMask = cv2.GaussianBlur(skinMask, (5, 5), 0)

    # Extracting skin from the threshold mask
    skin = cv2.bitwise_and(HSV, HSV, mask=skinMask)


    # MaskImage convert gray

    BGR = cv2.cvtColor(skin, cv2.COLOR_HSV2BGR)
    gray = cv2.cvtColor(BGR, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # noise removal
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)

    # sure background area
    sure_bg = cv2.dilate(opening, kernel, iterations=3)

    # Finding sure foreground area

    # Finding unknown region
    sure_fg = np.uint8(sure_bg)
    unknown = cv2.subtract(sure_bg, sure_fg)

    # Marker labelling
    #ret, markers = cv2.connectedComponents(sure_fg,8)

    n_labels, markers, lab_stats, GoCs = cv2.connectedComponentsWithStats(sure_fg, connectivity=8, ltype=cv2.CV_32S)
    ##########################################################################################
    hand_space = np.zeros(markers.shape)
    contours, hierarchy = cv2.findContours(sure_fg, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for i in range(0,len(contours)):

        #print(i,"번쨰 값",contours[i])
        print(i,"번째 면적",cv2.arcLength(contours[i],True))

        print(i,"번째 둘레",cv2.arcLength(contours[i],False))
        measure = cv2.arcLength(contours[i],True)
        Circu = cv2.arcLength(contours[i],False)
        if measure > 2000: #면적이 1500픽셀 이상의 값만 출력
            if measure == 2996.0 and Circu == 2037.0:
                break
            mmt = cv2.moments(contours[i])
            print(i)
            cx = int(mmt['m10']/mmt['m00'])
            cy = int(mmt['m01']/mmt['m00'])
            print("(",cx," , ",cy,")")
            midPoint = cv2.circle(hand_space,(int(cx),int(cy)),1,(255,255,255),10)
            
            cv2.drawContours(hand_space, contours[i], -1, (255, 255, 255), 10)




    # Add one to all labels so that sure background is not 0, but 1
    markers = markers + 1

    # Now, mark the region of unknown with zero
    markers[unknown == 255] = 0

    markers = cv2.watershed(hand, markers)

    hand[markers == -1] = [255, 0, 0]
    hand[markers == 1] = [255, 255, 0]
    #ret1, threshold1 = cv2.threshold(gray, 80, 255, cv2.THRESH_BINARY)
    #cv2.imshow("dist_transform", DefaultWindowResize(result_dist_transform))
    #cv2.imshow("unknown", DefaultWindowResize(unknown))
    cv2.imshow("sure_fg", DefaultWindowResize(sure_fg))
    cv2.imshow("sure_bg", DefaultWindowResize(sure_bg))
    cv2.imshow("result", DefaultWindowResize(hand))
    cv2.imshow("make", DefaultWindowResize(skinMask))
    cv2.imshow("Black",DefaultWindowResize(hand_space))
    '''threshold1 = cv2.bitwise_not(threshold1)

    kernel = np.ones((5,5),np.uint8)
    erosion = cv2.erode(threshold1,kernel,iterations=1)
    #cv2.imshow("erode",DefaultWindowResize(erosion))

    dilation = cv2.dilate(threshold1,kernel,iterations=3)
    #cv2.imshow("dilate",DefaultWindowResize(dilation))


    opening = cv2.morphologyEx(threshold1,cv2.MORPH_OPEN,kernel)
    #cv2.imshow("opening",DefaultWindowResize(opening))

    edges = cv2.Canny(dilation, 120, 220)
    #cv2.imshow("edge",DefaultWindowResize(edges))

    #########################################################################
    ret3,labels = cv2.connectedComponents(edges)
    label_hue = np.uint8(179 * labels / np.max(labels))
    blank_ch = 255 * np.ones_like(label_hue)
    labeled_img = cv2.merge([label_hue, blank_ch, blank_ch])

    # cvt to BGR for display
    labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)

    # set bg label to black
    labeled_img[label_hue == 0] = 0
    #cv2.imshow("labelimg",DefaultWindowResize(labeled_img))

    ######################################################################### Using DepthMap
    cv2.imshow("Gray",DefaultWindowResize(gray))
    #stereo = cv2.StereoBM_create(numDisparities=16,blockSize=15)
    #cv2.imshow("stereo",DefaultWindowResize(stereo))
    cv2.imshow("Threshold",DefaultWindowResize(threshold1))
    ######################################################################### Using labeling
    n_labels,img_labeled,lab_stats,_ = cv2.connectedComponentsWithStats(threshold1,connectivity=8,ltype=cv2.CV_32S)
    : 라벨링함수.binary_image1: 레이블링 할 이미지, connectivity: 레이블링 된 이미지의 중심 좌표, n_labels: 라벨번호, 
    img_labeled: 각 레이블링 부분의 이미지 배열, lab_stats: 모두 레이블링 된 이미지 배열

    print(img_labeled)


    #########################################################################
    #markers = cv2.subtract(erosion,dilation)'''

    #cv2.imshow("Marker",DefaultWindowResize(markers))
    #cv2.imshow("original",DefaultWindowResize(hand))
    #cv2.imshow("mask",DefaultWindowResize(skin))
    #cv2.imshow("threshold",DefaultWindowResize(threshold1))

    if cv2.waitKey(1) & 0xff == ord('q'):  # q버튼을 누르면 영상 꺼짐
        break
cap.release()
cv2.destroyAllWindows()

# -*- coding: cp949
# -*- coding: euc-kr


import cv2
import matplotlib.pyplot as plt
import numpy as np
import turtle as t



def showimage():
    img_f='testimg.jpg'
    img=cv2.imread(img_f,cv2.IMREAD_GRAYSCALE)
    cv2.namedWindow('Suzy',cv2.WINDOW_AUTOSIZE)
    cv2.imshow('Suzy',img)


    k=cv2.waitKey(0)&0xFF
    
    while(1):
        if k==27:
            cv2.destroyAllWindows()
            break;
        elif k==ord('c'):
            cv2.imwrite('testimg_copy.jpg',img)
            cv2.destroyAllWindows()
            break;
        
def plotimage():
    img_f='testimg.jpg'
    img=cv2.imread(img_f,cv2.IMREAD_GRAYSCALE)

    plt.imshow(img, cmap='gray', interpolation='bicubic')
    plt.xticks([])
    plt.yticks([])
    plt.title('suzy')
    plt.show()
         

plotimage()     
showimage()


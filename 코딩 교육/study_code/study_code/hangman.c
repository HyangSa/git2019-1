#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int createOneDigitAnswer();
int guessOneDigit(int, int);
int show(int[], int[]);
int isDone(int[]);
int printAnswer(int[]);

int main() {
	srand(time(NULL));
	int i, try;
	int answer[6];
	int foundAnswer[6]; // 해당 자리의 숫자를 사용자가 맞추면 1, 아니면 0

	for (i = 0; i < 6; i++) {
		answer[i] = createOneDigitAnswer();
		foundAnswer[i] = 0; // 처음에는 맞추기 전이므로 모두 0으로 설정
	}

	try = 8;

	while (try > 0 && isDone(foundAnswer) == 0) {
		int guess;
		show(foundAnswer, answer);

		printf("\n");
		printf("You have %d chances left.\n", try);
		printf("Make a guess between 0~9:");
		scanf("%d", &guess);

		for (i = 0; i < 6; i++) {
			if (guessOneDigit(guess, answer[i])) {
				foundAnswer[i] = guess;
			}

		}
		try--;
	}
	show(foundAnswer, answer);

	if (isDone(foundAnswer)) {
		printf("CONGRATULATIONS!\n");
		printAnswer(answer);
	}
	else {
		printf("YOU FAILED TO ANSWER!\n");
		printAnswer(answer);
	}
	return 0;
}

int createOneDigitAnswer() {
	// 한 개의 랜덤 숫자를 만들어 주는 함수
	// 파라미터: 없음
	// 리턴: 0~9 사이의 랜덤 숫자 하나
	// 힌트: rand()%n 을 하면 0~n-1까지의 숫자 중 랜덤 숫자가 배정된다.
	
	return rand() % 10;
	//여기에 코드를 추가하세요!!
}

int show(int foundAnswer[], int answer[]) {
	// 현재 게임 상황을 보여주는 함수
	// 파라미터:  두개의 int[] 배열, foundAnswer와 answer(둘 다 길이 6)
	// 리턴: 게임 상황을 printf로 다 보여준 후 아무 숫자나 리턴(현재는 0)
	// 힌트: for문을 돌면서 if문과 foundAnswer를 이용해 뭘 출력할지 결정

	int i;
	printf("\n");
	printf("---HANGMAN GAME---\n");
	printf("  ");
	for (i = 0; i < 6; i++) {
		printf(" ");
		if (foundAnswer[i]) {
			printf("%d", foundAnswer[i]);
		}
		else {
			printf("_");
		}
	}
	printf("\n");
	//여기에 코드를 추가하세요!!
	//예시 답안 참고
	//이미 맞춘 숫자는 숫자로, 그렇지 않은 숫자는 _로 표시
	//스페이스 규격 예시 답안 참고하여 맞출 것

	printf("------------------\n");
	return 0;
}

int guessOneDigit(int guess, int answerDigit) {
	// 입력된 숫자가 답의 digit과 같은지 판별하는 함수
	// 파라미터:  두개의 int, 사용자가 찍은 숫자와 실제 정답의 특정 자릿수
	// 리턴: 두 숫자가 같으면 1, 다르면 0을 리턴

	if (guess == answerDigit) {
		return 1;
	}
	return 0;

	//여기에 코드를 추가하세요!!
}

int isDone(int foundAnswer[]) {
	// 사용자가 정답을 모두 맞추었는지 판별하는 함수
	// 파라미터:  한개의 int 배열, foundAnswer[]
	// 리턴:  사용자가 정답을 모두 맞추었으면 1, 아니면 0을 리턴
	// 힌트: for문으로 foundAnswer의 내용 확인

	int i;
	for (i = 0; i < 6; i++) {
		if (foundAnswer[i] == 0) {
			return 0;
		}
	}
	return 1;

	//여기에 코드를 추가하세요!!
}

int printAnswer(int answer[]) {
	// 게임이 끝나고 정답을 알려주는 함수
	// 파라미터:  한개의 int 배열, answer[]
	// 리턴: 정답을 printf로 잘 표시해주고 아무 숫자나 리턴(현재는 0)
	// 힌트: for문으로 answer의 내용 확인

	printf("THE ANSWER WAS ");
	int i;
	for (i = 0; i < 6; i++) {
		printf("%d", answer[i]);
	}
	//여기에 코드를 추가하세요!!

	return 0;
}
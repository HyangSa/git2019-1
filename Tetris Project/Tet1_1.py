import time
import random

MT=[[1,0,0,0,0,0,0,0,0,0,0,1] for i in range(21)]# 메인 창
MT[20]=[1,1,1,1,1,1,1,1,1,1,1,1] # 메인 창 바닥
PT=[[0,0,0,0] for i in range(4)] # 현재 블럭
NT=[[0,0,0,0] for i in range(4)] # 다음 블럭


Z=[[2,2,0],   [0,2,2]] # 블럭 값 Z
S=[[0,2,2],   [2,2,0]] # 블럭 값 S
L=[[2,0,0],   [2,2,2]] # 블럭 값 ㄴ
J=[[0,0,2],   [2,2,2]] # 블럭 값 ┙
T=[[0,2,0],   [2,2,2]] # 블럭 값 ㅗ
M=[[0,2,2],   [0,2,2]] # 블럭 값 ㅁ
I=[[0,2],[0,2],[0,2],[0,2]] # 블럭 값 I
r=0
count   = 0  # 현재 블럭 출력용 변수
x_pos   = [] # 현재 블럭 x좌표
y_pos   = 0  # 현재 블럭 y좌표
ny_pos  = 0  # 다음 블럭 y좌표

def change(x,lst): # lst(4X4 리스트[PT,NT])에 x(블럭 값[Z~I]) 입력
    for i in range(4):
        for j in range(4):
##            print(i,j)
            if i<len(x) and j<len(x[0]):
                lst[i][j]=x[i][j]
            else :
                lst[i][j]=0

                
def nextblock(lst): # 블럭을 랜덤으로 지정
    global r
    r=random.randrange(0,7) #0~6
    if r==0:
        change(Z,lst) # ex change(Z,PT)=>PT리스트에 Z블럭값 입력
    elif r==1:
        change(S,lst)
    elif r==2:
        change(L,lst)
    elif r==3:
        change(J,lst)
    elif r==4:
        change(T,lst)
    elif r==5:
        change(M,lst)
    elif r==6:
        change(I,lst)
##    [print(NT[i]) for i in range(-1,-5,-1)]
##    print()
        
def gameover(y_pos): # 각 모형의 아랫부분이 1이면 더이상 내려갈수 없다고 판단
    if len(x_pos)==1:
        if MT[y_pos+1][x_pos[0]]==1: return 1; # 판단이 참이면 1를 반환하여 break
    elif len(x_pos)==2:
        if MT[y_pos+1][x_pos[0]]==1 or MT[y_pos+1][x_pos[1]]==1: return 1;
    elif len(x_pos)==3:
        if MT[y_pos+1][x_pos[0]]==1 or MT[y_pos+1][x_pos[1]]==1 or MT[y_pos+1][x_pos[2]]==1: return 1;
    

def findy(): # 현재 블럭 리스트의 블럭 길이 반환
    for i in range(15,-1,-1):
        if PT[i//4][i%4] == 2: return i//4
        
def blockin(): # 현재 블럭을 메인창에 1줄씩 입력
    global y_pos
    startline=0 # 시작점
    y_pos=0 # 블럭 y좌표 초기화
    count=findy() # 블럭길이 반환
    if gameover(-1)==1: return 1 # 첫줄이 막혀있는지 확인
    while count>=0: 
        MT[startline][4:8]=PT[count]
        print(count,y_pos)
        [print(PT[i],"|",NT[i]) for i in range(0,4)]
        print()
        [print(MT[i]) for i in range(21)]
        print()
        time.sleep(0.2)
        if gameover(y_pos)==1: return 1 # 다음 줄이 막혀있는지 확인
        if count>=1:
            drop()
        count=count-1
        
    
def drop():
    global y_pos
    y_pos+=1
    for i in range(len(MT)-1,-1,-1):
        for j in range(len(MT[0])-1,-1,-1):
            if MT[i][j]==2:
                if MT[i+1][j]!=1 or MT[i-1][j]!=2:
                    MT[i+1][j]=MT[i][j]
                    MT[i][j]=0
def pos():
    global x_pos,y_pos
    x_pos=[i for i, x in enumerate(MT[y_pos]) if x==2]
    print(x_pos, y_pos)
    [print(PT[i],"|",NT[i]) for i in range(0,4)]
    print()
    [print(MT[i]) for i in range(21)]
    print()
    time.sleep(0.2)
    
def dropdisplay():
    global x_pos,y_pos
    x_pos=[i for i, x in enumerate(MT[y_pos]) if x==2]
    if len(x_pos)==1:
        while MT[y_pos+1][x_pos[0]]!=1:
            drop()
            pos()
    elif len(x_pos)==2:
        while MT[y_pos+1][x_pos[0]]!=1 and MT[y_pos+1][x_pos[1]]!=1:
            drop()
            pos()
    elif len(x_pos)==3:
        while MT[y_pos+1][x_pos[0]]!=1 and MT[y_pos+1][x_pos[1]]!=1 and\
              MT[y_pos+1][x_pos[2]]!=1:
            drop()
            pos()
    elif len(x_pos)==4:
        while MT[y_pos+1][x_pos[0]]!=1 and MT[y_pos+1][x_pos[1]]!=1 and\
              MT[y_pos+1][x_pos[2]]!=1 and MT[y_pos+1][x_pos[3]]!=1:
            drop()
            pos()
    for i in range(len(MT)-1,-1,-1):
        for j in range(len(MT[0])-1,-1,-1):
            if MT[i][j]==2:
                MT[i][j]=1
    for i in range(4):
        for j in range(4):
            PT[i][j]=NT[i][j]

    
if __name__ == '__main__':
    global r,inypos,ny_pos,y_pos
    nextblock(PT)
    while(1):
        nextblock(NT)
        if blockin()==1: break
        dropdisplay()
        

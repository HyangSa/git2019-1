import time
import random
import keyboard

MainT    =[[1,0,0,0,0,0,0,0,0,0,0,1] for i in range(23)] # 메인 창
MainT[22]= [1,1,1,1,1,1,1,1,1,1,1,1]     # 메인 창 바닥
presentT =[[0,0,0,0] for i in range(4)] # 현재 블럭
NextT    =[[0,0,0,0] for i in range(4)] # 다음 블럭

Z=[[2,2,0],   [0,2,2]] # 블럭 값 Z
S=[[0,2,2],   [2,2,0]] # 블럭 값 S
L=[[2,0,0],   [2,2,2]] # 블럭 값 ㄴ
J=[[0,0,2],   [2,2,2]] # 블럭 값 ┙
T=[[0,2,0],   [2,2,2]] # 블럭 값 ㅗ
M=[[0,2,2],   [0,2,2]] # 블럭 값 ㅁ
I=[[0,2],[0,2],[0,2],[0,2]] # 블럭 값 I
PBnum,NBnum=0,0
Bstate=1
count   = 0  # 현재 블럭 출력용 변수
pos     = []
flag    = 1       # 상태 변수

def change(x,lst): # lst(4X4 리스트[presentT,NextT])에 x(블럭 값[Z~I]) 입력
    for i in range(4):
        for j in range(4):
##            print(i,j)
            if i<len(x) and j<len(x[0]): # i<x행, j<x열
                lst[i][j]=x[i][j]
            else :
                lst[i][j]=0

def SetBlock(lst): # 블럭을 랜덤으로 지정
    r=random.randrange(0,7) #0~6
    if r==0:
        change(Z,lst) # ex change(Z,presentT)=>PT리스트에 Z블럭값 입력
        return 1
    elif r==1:
        change(S,lst)
        return 2
    elif r==2:
        change(L,lst)
        return 3
    elif r==3:
        change(J,lst)
        return 4
    elif r==4:
        change(T,lst)
        return 5
    elif r==5:
        change(M,lst)
        return 6
    elif r==6:
        change(I,lst)
        return 7

def LineCheck(pos): # 각 모형의 아랫부분이 1이면 더이상 내려갈수 없다고 판단
    if len(pos)==0:
        if MainT[0][5]==1: return 1; # 판단이 참이면 1를 반환(다음단계)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        else: return 0 # 거짓이면 0 반환 (반복)
    elif len(pos)==1:
        if MainT[pos[0][0]+1][pos[0][1]]==1: return 1;
        else: return 0
    elif len(pos)==2:
        if MainT[pos[0][0]+1][pos[0][1]]==1 or MainT[pos[1][0]+1][pos[1][1]]==1: return 1
        else: return 0
    elif len(pos)==3:
        if MainT[pos[0][0]+1][pos[0][1]]==1 or MainT[pos[1][0]+1][pos[1][1]]==1 or\
           MainT[pos[2][0]+1][pos[2][1]]==1: return 1
        else: return 0
    elif len(pos)==4:
        if MainT[pos[0][0]+1][pos[0][1]]==1 or MainT[pos[1][0]+1][pos[1][1]]==1 or\
           MainT[pos[2][0]+1][pos[2][1]]==1 or MainT[pos[3][0]+1][pos[3][1]]==1: return 1
        else: return 0
    

def findy(): # 현재 블럭 리스트의 블럭 길이 반환
    for i in range(15,-1,-1):
        if presentT[i//4][i%4] == 2: return i//4

def findblock(): # 현재 블럭 리스트의 블럭 길이 반환
    x,y=[],[]
    for i in range(len(MainT)-1,-1,-1):
        for j in range(0,len(MainT[0])):
            if MainT[i][j]==2:
                x.append(j)
                y.append(i)
    return list(zip(y,x))
        
def Blockin(): # 현재 블럭을 메인창에 1줄씩 입력
    global pos
    
    if LineCheck([])==1: return 1 # 첫줄이 막혀있는지 확인
    count=findy() # 블럭길이 반환
    while count>=0: # 블럭 길이만큼 메인 창에 입력
        MainT[0][4:8]=presentT[count] # 첫줄에 현재블럭 맨 아래부분 입력
        #print(count) # 남은 블럭길이 출력(확인용)
        
        pos=findblock()
        if LineCheck(pos)==1: return 1 # 다음 줄이 막혀있는지 확인
        if count>=1: # 블럭의 남은 줄이 1보다 크거나 같으면
            Drop() # 블럭 내리기(1칸)
##            time.sleep(0.1)
        count=count-1 # 블럭 줄 - 1
    Print_Pos() # 리스트 출력
        

def Print_Pos(): # 현재 상태 출력
    global pos,PBnum
    print(pos,PBnum) # 블럭 X,Y
    [print(presentT[i],"|",NextT[i]) for i in range(0,4)] # 현재 블럭모양, 다음 블럭모양
    print()
    [print(MainT[i]) for i in range(2,23)] # 메인 창
    print()
    
def Down_or_Set(): # 상태 확인(더이상 내려갈 수 없는지) 후 다음 코드 실행
    global flag,pos,PBnum,NBnum,Bstate
    
    Print_Pos()
    flag=LineCheck(pos) # gameover함수로 블럭 아래 확인 후 flag 지정
   
    if flag==1: # 1이면 내려갈 곳이 없음 => 블럭 값을 1로 바꿈
        for i in range(len(MainT)-1,-1,-1):
            for j in range(len(MainT[0])-1,-1,-1):
                if MainT[i][j]==2:
                    MainT[i][j]=1
        for i in range(4):
            for j in range(4):
                presentT[i][j]=NextT[i][j]
        for i in range(len(MainT)-2,2,-1):
            Full()
        PBnum=NBnum
    
    
def Full(): # 메인 창에 1로 모두 차면 그 윗부분을 모두 1칸씩 내림
    for i in range(len(MainT)-2,2,-1):
        if MainT[i][1:11]==[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]:
            MainT[i][1:11]=[9, 9, 9, 9, 9, 9, 9, 9, 9, 9]
            for j in range(i,2,-1):
                MainT[j][1:11]=MainT[j-1][1:11]

def Drop(): # 리스트에서 2인 값만 1줄 내리고 0으로 채움
    global pos
    for i in range(len(MainT)-1,-1,-1):
        for j in range(len(MainT[0])-1,-1,-1):
            if MainT[i][j]==2:
                if MainT[i+1][j]!=1 or MainT[i-1][j]!=2:
                    MainT[i+1][j]=MainT[i][j]
                    MainT[i][j]=0

def Left(): # 블럭 좌표 좌측 확인 후 1칸이동
    global pos
    if MainT[pos[0][0]][pos[0][1]-1]!=1 and MainT[pos[1][0]][pos[1][1]-1]!=1 and\
       MainT[pos[2][0]][pos[2][1]-1]!=1 and MainT[pos[3][0]][pos[3][1]-1]!=1:
        for i in range(1,len(MainT)):
            for j in range(1,len(MainT[0])):
                if MainT[i][j]==2:
                    MainT[i][j-1]=MainT[i][j]
                    MainT[i][j]=0

def Right(): # 블럭 좌표 우측 확인 후 1칸이동
    global pos
    if MainT[pos[0][0]][pos[0][1]+1]!=1 and MainT[pos[1][0]][pos[1][1]+1]!=1 and\
       MainT[pos[2][0]][pos[2][1]+1]!=1 and MainT[pos[3][0]][pos[3][1]+1]!=1:
        for i in range(1,len(MainT)):
            for j in range(len(MainT[0])-2,0,-1):
                if MainT[i][j]==2:
                    MainT[i][j+1]=MainT[i][j]
                    MainT[i][j]=0
def Rotate(): # 각 블럭마다 PBnum, 모양 단계별로 좌표 확인 후 회전 
    global PBnum,Bstate,pos
    if PBnum==7: # I모양
        if Bstate==1:
            if MainT[pos[1][0]][pos[1][1]-1]!=1 and MainT[pos[1][0]][pos[1][1]]!=1 and\
               MainT[pos[1][0]][pos[1][1]+1]!=1 and MainT[pos[1][0]][pos[1][1]+2]!=1:
                for i in range(-1,3):
                    MainT[pos[i+1][0]][pos[i+1][1]]=0
                    MainT[pos[1][0]][pos[1][1]+i]=2
                Bstate=2
        elif Bstate==2:
            if MainT[pos[1][0]-2][pos[1][1]]!=1 and MainT[pos[1][0]-1][pos[1][1]]!=1 and\
               MainT[pos[1][0]][pos[1][1]]!=1 and MainT[pos[1][0]+1][pos[1][1]]!=1:
                for i in range(-2,2):
                    MainT[pos[i+2][0]][pos[i+2][1]]=0
                    MainT[pos[1][0]+i][pos[1][1]]=2
                Bstate=1
    elif PBnum==1: # Z 모양
        if Bstate==1:
            if MainT[pos[0][0]+1][pos[0][1]]!=1 and MainT[pos[1][0]-1][pos[1][1]]!=1:
                MainT[pos[2][0]][pos[2][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[0][0]+1][pos[0][1]],MainT[pos[1][0]-1][pos[1][1]]=2,2
                Bstate=2
        elif Bstate==2:
            if MainT[pos[3][0]][pos[3][1]-2]!=1 and MainT[pos[3][0]][pos[3][1]-1]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[3][0]][pos[3][1]-2],MainT[pos[3][0]][pos[3][1]-1]=2,2
                Bstate=1
    elif PBnum==2: # S 모양
        if Bstate==1:
            if MainT[pos[0][0]-1][pos[0][1]]!=1 and MainT[pos[1][0]+1][pos[1][1]]!=1:
                MainT[pos[2][0]][pos[2][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[0][0]-1][pos[0][1]],MainT[pos[1][0]+1][pos[1][1]]=2,2
                Bstate=2
        elif Bstate==2:
            if MainT[pos[3][0]][pos[3][1]+1]!=1 and MainT[pos[3][0]][pos[3][1]+2]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[3][0]][pos[3][1]+1],MainT[pos[3][0]][pos[3][1]+2]=2,2
                Bstate=1
    elif PBnum==3: # L모양
        if Bstate==1:
            if MainT[pos[0][0]+1][pos[0][1]]!=1 and MainT[pos[0][0]-1][pos[0][1]+1]!=1:
                MainT[pos[1][0]][pos[1][1]],MainT[pos[2][0]][pos[2][1]]=0,0
                MainT[pos[0][0]+1][pos[0][1]],MainT[pos[0][0]-1][pos[0][1]+1]=2,2
                Bstate=2
        elif Bstate==2:
            if MainT[pos[3][0]][pos[3][1]+1]!=1 and MainT[pos[3][0]+1][pos[3][1]+1]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[1][0]][pos[1][1]]=0,0
                MainT[pos[3][0]][pos[3][1]+1],MainT[pos[3][0]+1][pos[3][1]+1]=2,2
                Bstate=3
        elif Bstate==3:
            if MainT[pos[0][0]][pos[0][1]-1]!=1 and MainT[pos[3][0]-1][pos[3][1]]!=1:
                MainT[pos[1][0]][pos[1][1]],MainT[pos[2][0]][pos[2][1]]=0,0
                MainT[pos[0][0]][pos[0][1]-1],MainT[pos[3][0]-1][pos[3][1]]=2,2
                Bstate=4
        elif Bstate==4:
            if MainT[pos[0][0]-1][pos[0][1]]!=1 and MainT[pos[1][0]][pos[1][1]+1]!=1:
                MainT[pos[2][0]][pos[2][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[0][0]-1][pos[0][1]],MainT[pos[1][0]][pos[1][1]+1]=2,2
                Bstate=1
    elif PBnum==4: # J 모양
        if Bstate==1:
            if MainT[pos[1][0]-1][pos[1][1]]!=1 and MainT[pos[1][0]-2][pos[1][1]]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[1][0]-1][pos[1][1]],MainT[pos[1][0]-2][pos[1][1]]=2,2
                Bstate=2
        elif Bstate==2:
            if MainT[pos[0][0]][pos[0][1]-1]!=1 and MainT[pos[2][0]][pos[2][1]-1]!=1 and\
               MainT[pos[2][0]][pos[2][1]+1]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[1][0]][pos[1][1]],MainT[pos[3][0]][pos[3][1]]=0,0,0
                MainT[pos[0][0]][pos[0][1]-1],MainT[pos[2][0]][pos[2][1]-1],MainT[pos[2][0]][pos[2][1]+1]=2,2,2
                Bstate=3
        elif Bstate==3:
            if MainT[pos[2][0]-1][pos[2][1]]!=1 and MainT[pos[3][0]+1][pos[3][1]]!=1 and\
               MainT[pos[3][0]-1][pos[3][1]]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[1][0]][pos[1][1]],MainT[pos[2][0]][pos[2][1]]=0,0,0
                MainT[pos[2][0]-1][pos[2][1]],MainT[pos[3][0]-1][pos[3][1]],MainT[pos[3][0]+1][pos[3][1]]=2,2,2
                Bstate=4
        elif Bstate==4:
            if MainT[pos[0][0]][pos[0][1]-2]!=1 and MainT[pos[0][0]][pos[0][1]-1]!=1:
                MainT[pos[2][0]][pos[2][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[0][0]][pos[0][1]-2],MainT[pos[0][0]][pos[0][1]-1]=2,2
                Bstate=1
    elif PBnum==5: # T 모양
        if Bstate==1:
            if MainT[pos[0][0]-2][pos[0][1]]!=1 and MainT[pos[0][0]-1][pos[0][1]]!=1:
                MainT[pos[1][0]][pos[1][1]],MainT[pos[2][0]][pos[2][1]]=0,0
                MainT[pos[0][0]-2][pos[0][1]],MainT[pos[0][0]-1][pos[0][1]]=2,2
                Bstate=2
        elif Bstate==2:
            if MainT[pos[2][0]][pos[2][1]+1]!=1 and MainT[pos[2][0]+1][pos[2][1]]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[2][0]][pos[2][1]+1],MainT[pos[2][0]+1][pos[2][1]]=2,2
                Bstate=3
        elif Bstate==3:
            if MainT[pos[3][0]-1][pos[3][1]]!=1 and MainT[pos[3][0]+1][pos[3][1]]!=1:
                MainT[pos[0][0]][pos[0][1]],MainT[pos[1][0]][pos[1][1]]=0,0
                MainT[pos[3][0]-1][pos[3][1]],MainT[pos[3][0]+1][pos[3][1]]=2,2
                Bstate=4
        elif Bstate==4:
            if MainT[pos[0][0]][pos[0][1]-2]!=1 and MainT[pos[0][0]][pos[0][1]-1]!=1:
                MainT[pos[2][0]][pos[2][1]],MainT[pos[3][0]][pos[3][1]]=0,0
                MainT[pos[0][0]][pos[0][1]-2],MainT[pos[0][0]][pos[0][1]-1]=2,2
                Bstate=1
    
    
def inputButton(): # 0.5초 미입력시 내리기
    t=time.time()
    while True:
        if keyboard.is_pressed('4'):
            Left() # 4 입력시 좌측이동
            break
        elif keyboard.is_pressed('6'):
            Right() # 6 입력시 우측 이동
            break
        elif keyboard.is_pressed('5'):
            Drop() # 5 입력시 내리기
            break
        elif keyboard.is_pressed('8'):
            Rotate() # 8 입력시 회전
            break
        if time.time()-t>=0.5:
            Drop()
            break
        
if __name__ == '__main__': # 메인 함수
    PBnum=SetBlock(presentT) # 현재 블럭 지정.(첫 블럭 초기화)
    while True: # 반복
        inputButton() # 키입력(이동,회전)
        pos=findblock() # 좌표 확인
        if flag==1: # (flag 1로 시작함) 및 현재 블럭을 메인창으로 내림
            NBnum=SetBlock(NextT) #  다음 블럭 지정
            Bstate=1 # 회전 단계 초기화
            if Blockin()==1: break # 현재 블럭을 메인창으로 내림 # *내리는 중 내릴수 없다 판단시 게임 종료
        Down_or_Set() # 상태 확인 및 진행.


import time
import random
import keyboard

MainT=[[1,0,0,0,0,0,0,0,0,0,0,1] for i in range(21)]# 메인 창
MainT[20]=[1,1,1,1,1,1,1,1,1,1,1,1] # 메인 창 바닥
presentT=[[0,0,0,0] for i in range(4)] # 현재 블럭
NextT=[[0,0,0,0] for i in range(4)] # 다음 블럭


Z=[[2,2,0],   [0,2,2]] # 블럭 값 Z
S=[[0,2,2],   [2,2,0]] # 블럭 값 S
L=[[2,0,0],   [2,2,2]] # 블럭 값 ㄴ
J=[[0,0,2],   [2,2,2]] # 블럭 값 ┙
T=[[0,2,0],   [2,2,2]] # 블럭 값 ㅗ
M=[[0,2,2],   [0,2,2]] # 블럭 값 ㅁ
I=[[0,2],[0,2],[0,2],[0,2]] # 블럭 값 I
count   = 0  # 현재 블럭 출력용 변수
x_pos   = [] # 현재 블럭 x좌표
y_pos   = 0  # 현재 블럭 y좌표
flag=1       # 상태 변수

def change(x,lst): # lst(4X4 리스트[presentT,NextT])에 x(블럭 값[Z~I]) 입력
    for i in range(4):
        for j in range(4):
##            print(i,j)
            if i<len(x) and j<len(x[0]): # i<x행, j<x열
                lst[i][j]=x[i][j]
            else :
                lst[i][j]=0

def SetBlock(lst): # 블럭을 랜덤으로 지정
    r=random.randrange(0,7) #0~6
    if r==0:
        change(Z,lst) # ex change(Z,presentT)=>PT리스트에 Z블럭값 입력
    elif r==1:
        change(S,lst)
    elif r==2:
        change(L,lst)
    elif r==3:
        change(J,lst)
    elif r==4:
        change(T,lst)
    elif r==5:
        change(M,lst)
    elif r==6:
        change(I,lst)
##    [print(NextT[i]) for i in range(-1,-5,-1)]
##    print()
        
def LineCheck(y_pos): # 각 모형의 아랫부분이 1이면 더이상 내려갈수 없다고 판단
    global x_pos
    x_pos=[i for i, x in enumerate(MainT[y_pos]) if x==2]
    if len(x_pos)==1:
        if MainT[y_pos+1][x_pos[0]]==1: return 1; # 판단이 참이면 1를 반환(다음단계)
        else: return 0 # 거짓이면 0 반환 (반복)
    elif len(x_pos)==2:
        if MainT[y_pos+1][x_pos[0]]==1 or MainT[y_pos+1][x_pos[1]]==1: return 1
        else: return 0
    elif len(x_pos)==3:
        if MainT[y_pos+1][x_pos[0]]==1 or MainT[y_pos+1][x_pos[1]]==1 or MainT[y_pos+1][x_pos[2]]==1: return 1
        else: return 0
    

def findy(): # 현재 블럭 리스트의 블럭 길이 반환
    for i in range(15,-1,-1):
        if presentT[i//4][i%4] == 2: return i//4
        
def Blockin(): # 현재 블럭을 메인창에 1줄씩 입력
    global y_pos
    if LineCheck(-1)==1: return 1 # 첫줄이 막혀있는지 확인
    startline=0 # 시작점
    y_pos=0 # 블럭 y좌표 초기화
    count=findy() # 블럭길이 반환
    while count>=0: # 블럭 길이만큼 메인 창에 입력
        MainT[startline][4:8]=presentT[count] # 첫줄에 현재블럭 맨 아래부분 입력
        #print(count) # 남은 블럭길이 출력(확인용)
        Print_Pos() # 리스트 출력
        if LineCheck(y_pos)==1: return 1 # 다음 줄이 막혀있는지 확인
        if count>=1: # 블럭의 남은 줄이 1보다 크거나 같으면
            Drop() # 블럭 내리기(1칸)
            time.sleep(0.2)
        count=count-1 # 블럭 줄 - 1

def Drop(): # 리스트에서 2인 값만 1줄 내리고 0으로 채움
    global y_pos
    for i in range(len(MainT)-1,-1,-1):
        for j in range(len(MainT[0])-1,-1,-1):
            if MainT[i][j]==2:
                if MainT[i+1][j]!=1 or MainT[i-1][j]!=2:
                    MainT[i+1][j]=MainT[i][j]
                    MainT[i][j]=0
    y_pos+=1 # 블럭 y좌표 +1
    
def Up(): # 리스트에서 2인 값만 1줄 내리고 0으로 채움
    global y_pos
    for i in range(len(MainT)-1,-1,-1):
        for j in range(len(MainT[0])-1,-1,-1):
            if MainT[i][j]==2:
                MainT[i-1][j]=MainT[i][j]
                MainT[i][j]=0
    y_pos-=1 # 블럭 y좌표 +1

def Print_Pos(): # 현재 상태 출력
    global x_pos
    x_pos=[i for i, x in enumerate(MainT[y_pos]) if x==2]
    print(x_pos, y_pos) # 블럭 X,Y
    [print(presentT[i],"|",NextT[i]) for i in range(0,4)] # 현재 블럭모양, 다음 블럭모양
    print()
    [print(MainT[i]) for i in range(21)] # 메인 창
    print()
##    time.sleep(0.4) # 0.2초 대기
    
def Down_or_Set(): # 상태 확인(더이상 내려갈 수 없는지) 후 다음 코드 실행
    global flag
    flag=LineCheck(y_pos) # gameover함수로 블럭 아래 확인 후 flag 지정

    if flag==1: # 1이면 내려갈 곳이 없음 => 블럭 값을 1로 바꿈
        for i in range(len(MainT)-1,-1,-1):
            for j in range(len(MainT[0])-1,-1,-1):
                if MainT[i][j]==2:
                    MainT[i][j]=1
        for i in range(4):
            for j in range(4):
                presentT[i][j]=NextT[i][j]
                
    elif flag==0: # 0이면 더 내려갈 수 있음 => 2인 값 1줄 내리고 상태 출력
        Drop()
        Print_Pos()

def Left():
    global x_pos,y_pos
    if MainT[y_pos][x_pos[0]-1]!=1:
        for i in range(1,len(MainT)):
            for j in range(1,len(MainT[0])):
                if MainT[i][j]==2:
                    MainT[i-1][j-1]=MainT[i][j]
                    MainT[i][j]=0
        y_pos-=1
def Right():
    global x_pos,y_pos
    if MainT[y_pos][x_pos[-1]+1]!=1:
        for i in range(1,len(MainT)):
            for j in range(len(MainT[0])-2,0,-1):
                if MainT[i][j]==2:
                    MainT[i-1][j+1]=MainT[i][j]
                    MainT[i][j]=0
        y_pos-=1
def inputButton():
    t=time.time()
    while True:
        if keyboard.is_pressed('4'):
            Left()
            break
        elif keyboard.is_pressed('6'):
            Right()
            break
        elif keyboard.is_pressed('8'):
            break
        if time.time()-t>=0.5:
            break
        
if __name__ == '__main__': # 메인 함수
    SetBlock(presentT) # 현재 블럭 지정.(첫 블럭 초기화)
    
    while True: # 반복
        inputButton()
        if flag==1: # (flag 1로 시작함) 및 현재 블럭을 메인창으로 내림
            SetBlock(NextT) #  다음 블럭 지정
            if Blockin()==1: break # 현재 블럭을 메인창으로 내림 # *내리는 중 내릴수 없다 판단시 게임 종료
        Down_or_Set() # 상태 확인 및 진행.


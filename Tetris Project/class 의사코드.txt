class Tetris:
	##필드 선언##
	메인창# MainT=[]
	블럭 좌표값 # pos=[] 
	상태변수 # flag=1
	##메서드 선언##
	def __init__(self):
		self.메인창 초기화
		self.pos,flag 초기화
	def findy(self,List):
		블럭 리스트를 받아와 return 블럭길이
	def findblock(self):
		self.pos=self.메인창의 블럭 x,y 좌표
	def LineCheck(self,P):
		self.MainT에서 블럭이 내려갈수 있으면 return 0 
		self.MainT에서 블럭이 못내려가면 return 1	
	def add(self,other):
		블록출력용변수 #count=findy()
		1. 메인창 첫줄 막혔는지 확인 막히면 return 1 # LineCheck([])==1: return 1 
		2. self리스트(메인창)에 other리스트(블럭리스트) 1줄씩 입력
		3. 다음줄 막혔는지 확인 막히면 return 1 # LineCheck(self.pos)==1: return 1 
		4. 블럭이 메인창에 다들어올때까지만 반복
	def full(self):
		1줄이 다 1로 찼다면 제거 후 위에 고정된 블럭 내림
	def set(self,P,N):
		self.flag=LineCheck(self.pos) #1이면 다 내려갔다는 뜻
		if 다 내려갔으면
			1. 블럭고정
			2. self.full를 사용
			3. P객체의 블럭리스트를 N객체의 블럭리스트로 바꿈
			4. N객체 블럭변경
			5. if self.add(P) 의 리턴값이 1이면 return 1
	def printf(self):
		self.메인창 출력


class Block:
	##필드 선언##
	블럭 모양   # Z,S,L,J,T,M,I
	블럭리스트 # Blocklist
	블럭 상태   # Bstate
	블럭모양변수 #  Bnum
	##메서드 선언##
	def __init__(self):
		self.블럭리스트 초기화
		self.블럭 상태, 블럭모양변수 초기화
	def replace(self,other): # ex -> PT==MT
		self.블럭리스트 = other.블럭리스트
		self.블럭상태 = other.블럭상태
		self.블럭모양변수 = other.블럭모양변수
		other.SetBlock()
	def change(self,x): # SetBlock에 사용
		self.블럭리스트에 입력된 [x]블럭 모양(Z~I)입력
	def SetBlock(self):
		0 ~ 6 --> Z ~ I
		Z~I 중 랜덤 모양을 위 change함수를 사용해 self.블럭리스트 변경
		self.블럭모양변수 변경
		회전에 따라 바뀌는 self.블럭상태 초기화(1)
	def Printf(self):
		self.블럭리스트 출력
		
		
class Move():
	def __init__(self):
		pass
	def down(self,M):
		M.pos를 사용하여 M.MainT의 블록 한칸 내리기
	def left(self,M):
		M.pos를 사용하여 M.MainT의 블록 한칸 왼쪽으로
	def right(self,M):
		M.pos를 사용하여 M.MainT의 블록 한칸 오른쪽으로
	def rotate(self,M,B):  # ex => rotate(MT,PT)
		## --> B.Bnum B.Bstate
		Bnum, Bstate로 블럭 모양과 회전 상태 판단하고
		M.pos를 사용하여 M.MainT의 블록 회전
	def Push(self,M,B):
		키보드입력에 따라 Left, Right, down, rotate 함수 실행
############################



MT=Tetris()
PT=Block()
NT=Block()
Mv=Move()

if __name__==__main__:
	PT.SetBlock()
	NT.SetBlock()
	MT.add(PT)
	while True:
		if MT.set(PT,NT)==1: break
		Mv.Push(MT,PT)
		MT.findblock()
		PT.Printf()
		NT.Printf()
		MT.printf()
		
		
		
		
		
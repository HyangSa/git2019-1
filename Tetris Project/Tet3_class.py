import time
import random
import keyboard
class BlockShape: # 블럭속성 클래스
    blockshape=[]
    def Zshape(self):
        self.blockshape=[[[2,2,0],
                          [0,2,2]]] # 블럭 값 Z
        self.blockshape.append([[0,0,2],[0,2,2],[0,2,0]])
        self.blockshape.append([[0,0,0],[2,2,0],[0,2,2]])
        self.blockshape.append([[0,2,0],[2,2,0],[2,0,0]])
        return [self.blockshape,1]
    def Sshape(self):
        self.blockshape=[[[0,2,2],
                          [2,2,0]]] # 블럭 값 S
        self.blockshape.append([[0,2,0],[0,2,2],[0,0,2]])
        self.blockshape.append([[0,0,0],[0,2,2],[2,2,0]])
        self.blockshape.append([[2,0,0],[2,2,0],[0,2,0]])
        return [self.blockshape,2]
    def Lshape(self):
        self.blockshape=[[[2,0,0],
                          [2,2,2]]] # 블럭 값 ㄴ
        self.blockshape.append([[0,2,2],[0,2,0],[0,2,0]])
        self.blockshape.append([[0,0,0],[2,2,2],[0,0,2]])
        self.blockshape.append([[0,2,0],[0,2,0],[2,2,0]])
        return [self.blockshape,3]
    def Jshape(self):
        self.blockshape=[[[0,0,2],
                          [2,2,2]]] # 블럭 값 ┙
        self.blockshape.append([[0,2,0],[0,2,0],[0,2,2]])
        self.blockshape.append([[0,0,0],[2,2,2],[2,0,0]])
        self.blockshape.append([[2,2,0],[0,2,0],[0,2,0]])
        return [self.blockshape,4]
    def Tshape(self):
        self.blockshape=[[[0,2,0],
                          [2,2,2]]] # 블럭 값 ㅗ
        self.blockshape.append([[0,2,0],[0,2,2],[0,2,0]])
        self.blockshape.append([[0,0,0],[2,2,2],[0,2,0]])
        self.blockshape.append([[0,2,0],[2,2,0],[0,2,0]])
        return [self.blockshape,5]
    def Mshape(self):
        self.blockshape=[[[0,2,2],
                          [0,2,2]]] # 블럭 값 ㅁ
        self.blockshape.append([[0,2,2],[0,2,2]])
        self.blockshape.append([[0,2,2],[0,2,2]])
        self.blockshape.append([[0,2,2],[0,2,2]])
        return [self.blockshape,6]
    def Ishape(self):
        self.blockshape=[[[0,2],[0,2],[0,2],[0,2]]] # 블럭 값 I
        self.blockshape.append([[0,0,0,0],[2,2,2,2]])
        self.blockshape.append([[0,0,2],[0,0,2],[0,0,2],[0,0,2]])
        self.blockshape.append([[0,0,0,0],[0,0,0,0],[2,2,2,2]])
        return [self.blockshape,7]   

class Block(BlockShape): # 블럭 매니저 클래스
    Blocklist=[]
    Bnum=0
    Bstate=0
    bpos=[]
    def __init__(self): # 블럭 리스트 초기화
        self.Blocklist =[[[0,0,0,0] for i in range(4)] for j in range(4)] # 현재 블럭
    def setblock(self): # 사용시 랜덤한 블럭모양이 설정됨
        r=random.randrange(0,7)
        if r==0:
            self.change(self.Zshape()[0])
            self.Bnum= self.Zshape()[1]
        elif r==1:
            self.change(self.Sshape()[0])
            self.Bnum= self.Sshape()[1]
        elif r==2:
            self.change(self.Lshape()[0])
            self.Bnum= self.Lshape()[1]
        elif r==3:
            self.change(self.Jshape()[0])
            self.Bnum= self.Jshape()[1]
        elif r==4:
            self.change(self.Tshape()[0])
            self.Bnum= self.Tshape()[1]
        elif r==5:
            self.change(self.Mshape()[0])
            self.Bnum= self.Mshape()[1]
        elif r==6:
            self.change(self.Ishape()[0])
            self.Bnum= self.Ishape()[1]
        self.Bstate=0
        self.setbpos()
    def setbpos(self): # 랜덤블럭모양설정시 그에 맞게 블럭좌표 생성(setblock에서 사용)
        self.bpos=[]
        for k in range(4):
            lst=[]
            for s in range(16):
                i,j=s//4,s%4
                if self.Blocklist[k][i][j]==2:
                    lst.append((i,j))
            self.bpos.append(lst)
    def change(self,x): # 블럭모양을 리스트에 입력하기 위한 함수(setblock에서 사용)
        for k in range(4):
            for s in range(16):
                i,j=s//4,s%4
                if i<len(x[k]) and j<len(x[k][0]): # i<x행, j<x열
                    self.Blocklist[k][i][j]=x[k][i][j]
                else :
                    self.Blocklist[k][i][j]=0
    def replace(self,block): # 현재 객체의 블럭속성값을 입력받은 block 속성값으로 변경
        for k in range(4):
            for s in range(16):
                i,j=s//4,s%4
                self.Blocklist[k][i][j]=block.Blocklist[k][i][j]
        self.Bnum=block.Bnum
        self.Bstate=block.Bstate
        self.setbpos()
    def findy(self): # 현재 블럭 리스트의 블럭 길이 반환
        for i in range(15,-1,-1):
            if self.Blocklist[self.Bstate][i//4][i%4] == 2: return i//4
    def Printf(self): # 블럭 상태 shell 창에 출력
        print('Bnum=',self.Bnum,'Bstate=',self.Bstate)
        lst=[[0,0,0,0] for i in range(4)]
        for s in range(0,16):
            i,j=s//4,s%4
            if self.Blocklist[self.Bstate][i][j]==0:
                lst[i][j]="□"
            else:
                lst[i][j]="■"
        [print("".join([str(i) for i in lst[i]])) for i in range(0,4)]
##        [print(self.Blocklist[self.Bstate][i]) for i in range(0,4)]
        print()

class Move:
    
    def LineCheck(self,Ppos): # 각 모형의 아랫부분이 1이면 더이상 내려갈수 없다고 판단
        pos=Ppos
        MainT=self.MainT
        if len(pos)==0:
            if MainT[0][5]==1: return 1; # 판단이 참이면 1를 반환(다음단계)
            else: return 0 # 거짓이면 0 반환 (반복)
        elif len(pos)==1:
            if MainT[pos[0][0]+1][pos[0][1]]==1: return 1;
            else: return 0
        elif len(pos)==2:
            if MainT[pos[0][0]+1][pos[0][1]]==1 or MainT[pos[1][0]+1][pos[1][1]]==1: return 1
            else: return 0
        elif len(pos)==3:
            if MainT[pos[0][0]+1][pos[0][1]]==1 or MainT[pos[1][0]+1][pos[1][1]]==1 or\
               MainT[pos[2][0]+1][pos[2][1]]==1: return 1
            else: return 0
        elif len(pos)==4:
            if MainT[pos[0][0]+1][pos[0][1]]==1 or MainT[pos[1][0]+1][pos[1][1]]==1 or\
               MainT[pos[2][0]+1][pos[2][1]]==1 or MainT[pos[3][0]+1][pos[3][1]]==1: return 1
            else: return 0
            
    def Drop(self): # 리스트에서 2인 값만 1줄 내리고 0으로 채움
        pos=self.pos
        MainT=self.MainT
        for i in range(len(MainT)-1,-1,-1):
            for j in range(len(MainT[0])-1,-1,-1):
                if MainT[i][j]==2:
                    if MainT[i+1][j]!=1 or MainT[i-1][j]!=2:
                        MainT[i+1][j]=MainT[i][j]
                        MainT[i][j]=0
        self.y+=1

    def Left(self): # 블럭 좌표 좌측 확인 후 1칸이동
        pos=self.pos
        MainT=self.MainT
        if MainT[pos[0][0]][pos[0][1]-1]!=1 and MainT[pos[1][0]][pos[1][1]-1]!=1 and\
           MainT[pos[2][0]][pos[2][1]-1]!=1 and MainT[pos[3][0]][pos[3][1]-1]!=1:
            for i in range(1,len(MainT)):
                for j in range(1,len(MainT[0])):
                    if MainT[i][j]==2:
                        MainT[i][j-1]=MainT[i][j]
                        MainT[i][j]=0
            self.x-=1

    def Right(self): # 블럭 좌표 우측 확인 후 1칸이동
        pos=self.pos
        MainT=self.MainT
        if MainT[pos[0][0]][pos[0][1]+1]!=1 and MainT[pos[1][0]][pos[1][1]+1]!=1 and\
           MainT[pos[2][0]][pos[2][1]+1]!=1 and MainT[pos[3][0]][pos[3][1]+1]!=1:
            for i in range(1,len(MainT)):
                for j in range(len(MainT[0])-2,0,-1):
                    if MainT[i][j]==2:
                        MainT[i][j+1]=MainT[i][j]
                        MainT[i][j]=0
            self.x+=1   


    
class Tetris(Move): # 메인 창 클레스
    MainT=[[1,0,0,0,0,0,0,0,0,0,0,1] for i in range(23)] # 메인 창
    MainT[22]= [1,1,1,1,1,1,1,1,1,1,1,1]     # 메인 창 바닥
    pos=[]
    y=0
    x=4
    
    def __init__(self): # 우물 초기화
##        self.MainT    
##        self.
        pass
    def reset(self): # 좌표 초기화
        self.y=0
        self.x=4
    def findblock(self): # 우물 안 2인 값의 좌표 최신화
        x,y=[],[]
        for i in range(len(self.MainT)-1,-1,-1):
            for j in range(0,len(self.MainT[0])):
                if self.MainT[i][j]==2:
                    x.append(j)
                    y.append(i)
        self.pos=list(zip(y,x))

    def Printf(self): # shell창에 메인창 출력
        print('Pos=',self.pos)
        print('X,Y=',self.x,self.y)
        lst=[[1,0,0,0,0,0,0,0,0,0,0,1] for i in range(23)] 
        for j in range(0,12):
            for i in range(0,23):
                if self.MainT[i][j]==0:
                    lst[i][j]='□'
                else:
                    lst[i][j]='■'
        [print("".join([str(i) for i in lst[i]])) for i in range(2,23)]
##        [print(self.MainT[i]) for i in range(2,23)] # 메인 창
        print()
        




class Control: # 제어부분 클래스
    flag=0
    def __init__(self): # 
        pass
    def add_block(self,Main,Pblock): # 입력받은Pblock객체의 블럭리스트를 Main 중앙에 1줄씩 입력
        MainT=Main.MainT
        if Main.LineCheck([])==1: return 1 # 첫줄이 막혀있는지 확인
        count=Pblock.findy() # 블럭길이 반환
        while count>=0: # 블럭 길이만큼 메인 창에 입력
            MainT[0][4:8]=Pblock.Blocklist[Pblock.Bstate][count] # 첫줄에 현재블럭 맨 아래부분 입력
            #print(count) # 남은 블럭길이 출력(확인용)
            Main.findblock()
            if Main.LineCheck(Main.pos)==1: return 1 # 다음 줄이 막혀있는지 확인
            if count>=1: # 블럭의 남은 줄이 1보다 크거나 같으면
                for i in range(len(MainT)-1,-1,-1):
                    for j in range(len(MainT[0])-1,-1,-1):
                        if MainT[i][j]==2:
                            if MainT[i+1][j]!=1 or MainT[i-1][j]!=2:
                                MainT[i+1][j]=MainT[i][j]
                                MainT[i][j]=0
            count=count-1 # 블럭 줄 - 1
            
    def Full(self,Main): # Main의 우물에 한줄이 1로 모두 차면 지우고 그 윗부분을 모두 1칸씩 내림
        MainT=Main.MainT
        for i in range(len(MainT)-2,2,-1):
            if MainT[i][1:11]==[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]:
                MainT[i][1:11]=[9, 9, 9, 9, 9, 9, 9, 9, 9, 9]
                for j in range(i,2,-1):
                    MainT[j][1:11]=MainT[j-1][1:11]
                    
    def game_set(self,Main,Pblock,Nblock):  # Main에서 블럭이 다내려가면 블럭 고정, 위의 Full함수 실행, Main의 reset함수 실행
                                            # Pblock객체속성을 Nblock의 객체속성으로 바꾸고 Nblock객체 블럭속성 다시 지정
        self.flag=Main.LineCheck(Main.pos) # gameover함수로 블럭 아래 확인 후 flag 지정
        MainT=Main.MainT
        if self.flag==1: # 1이면 내려갈 곳이 없음 => 블럭 값을 1로 바꿈
            for i in range(len(MainT)-1,-1,-1):
                for j in range(len(MainT[0])-1,-1,-1):
                    if MainT[i][j]==2:
                        MainT[i][j]=1
            for i in range(len(MainT)-2,2,-1):
                self.Full(Main)
            Pblock.replace(Nblock)
            Nblock.setblock()
            Main.reset()
            if self.add_block(Main,Pblock)==1: return 1
            
    def Rotate(self,Main,Pblock): # 각 블럭마다 PBnum, 모양 단계별로 좌표 확인 후 회전 
        bpos=Pblock.bpos
        bstate=Pblock.Bstate+1
        if bstate==4: bstate=0
        MainT=Main.MainT
        if MainT[Main.y+bpos[bstate][0][0]][Main.x+bpos[bstate][0][1]]!=1 and\
           MainT[Main.y+bpos[bstate][1][0]][Main.x+bpos[bstate][1][1]]!=1 and\
           MainT[Main.y+bpos[bstate][2][0]][Main.x+bpos[bstate][2][1]]!=1 and\
           MainT[Main.y+bpos[bstate][3][0]][Main.x+bpos[bstate][3][1]]!=1:
            for i in range(0,4):
                MainT[Main.y+bpos[bstate-1][i][0]][Main.x+bpos[bstate-1][i][1]]=0
            for i in range(0,4):
                MainT[Main.y+bpos[bstate][i][0]][Main.x+bpos[bstate][i][1]]=2
            if Pblock.Bstate==3: Pblock.Bstate=0
            else : Pblock.Bstate+=1
    
    def Push(self,Main,Pblock): # 0.5초 미입력시 내리기
        t=time.time()
        while True:
            if keyboard.is_pressed('4'):
                Main.Left() # 4 입력시 좌측이동
                break
            elif keyboard.is_pressed('6'):
                Main.Right() # 6 입력시 우측 이동
                break
            elif keyboard.is_pressed('2'):
                Main.Drop() # 5 입력시 내리기
                break
            elif keyboard.is_pressed('8'):
                self.Rotate(Main,Pblock) # 8 입력시 회전
                break
            if time.time()-t>=0.5:
                Main.Drop()
                break

MT=Tetris()
CO=Control()
PT=Block()
NT=Block() 
if __name__ == '__main__': # 메인 함수
    PT.setblock()
    NT.setblock()
    CO.add_block(MT,PT)
##    t=time.time()
    while True: # 반복
        if CO.game_set(MT,PT,NT)==1:
            print('GAME OVER')
            break
        CO.Push(MT,PT) # 키입력(이동,회전)
        MT.findblock() # 좌표 확인
        NT.Printf()
        PT.Printf()
        MT.Printf()


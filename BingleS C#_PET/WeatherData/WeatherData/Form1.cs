﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 추가
using System.Net; 
using System.IO;
using System.Xml.Linq;

namespace WeatherData
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String strUrl = "http://www.kma.go.kr/weather/forecast/mid-term-xml.jsp";
            UriBuilder ub = new UriBuilder(strUrl);
            ub.Query = "srnLd=109"; //충청남도

            HttpWebRequest request;
            request = HttpWebRequest.Create(ub.Uri) as HttpWebRequest;
            request.BeginGetResponse(new AsyncCallback(GetResponse), request);
        }

        private void GetResponse(IAsyncResult ar)
        {
            HttpWebRequest wr = (HttpWebRequest)ar.AsyncState;
            HttpWebResponse wp = (HttpWebResponse)wr.EndGetResponse(ar);

            Stream stream = wp.GetResponseStream();
            StreamReader reader = new StreamReader(stream);

            String strRead = reader.ReadToEnd();
            XElement xmlMain = XElement.Parse(strRead);
            XElement xmlHead = xmlMain.Descendants("header").First();

            String strDesc = xmlHead.Element("wf").Value;
            strDesc = strDesc.Replace("<br />", "\n");
            String strTemp = strDesc + "\n";
            this.Invoke(new Action(() =>
            { textBox1.Text = strTemp; }
        ));
        }
    }
}

﻿namespace EPOR
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.ledon = new System.Windows.Forms.Button();
            this.lefoff = new System.Windows.Forms.Button();
            this.servoUP = new System.Windows.Forms.Button();
            this.servoCNT = new System.Windows.Forms.Button();
            this.servoDN = new System.Windows.Forms.Button();
            this.servoLEFT = new System.Windows.Forms.Button();
            this.servoRIGHT = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.printLCD1 = new System.Windows.Forms.Button();
            this.printLCD2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.motorRIGHT = new System.Windows.Forms.Button();
            this.motorLEFT = new System.Windows.Forms.Button();
            this.motorBWD = new System.Windows.Forms.Button();
            this.motorSTP = new System.Windows.Forms.Button();
            this.motorFWD = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Tone = new System.Windows.Forms.Button();
            this.oled = new System.Windows.Forms.Button();
            this.BTRemote = new System.Windows.Forms.Button();
            this.H_TRIM = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(364, 10);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(45, 21);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 180);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(139, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(236, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(314, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ready";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 263);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(454, 41);
            this.textBox2.TabIndex = 6;
            // 
            // ledon
            // 
            this.ledon.Location = new System.Drawing.Point(12, 235);
            this.ledon.Name = "ledon";
            this.ledon.Size = new System.Drawing.Size(75, 23);
            this.ledon.TabIndex = 7;
            this.ledon.Text = "LED ON";
            this.ledon.UseVisualStyleBackColor = true;
            this.ledon.Click += new System.EventHandler(this.ledon_Click);
            // 
            // lefoff
            // 
            this.lefoff.Location = new System.Drawing.Point(93, 235);
            this.lefoff.Name = "lefoff";
            this.lefoff.Size = new System.Drawing.Size(75, 23);
            this.lefoff.TabIndex = 8;
            this.lefoff.Text = "LED OFF";
            this.lefoff.UseVisualStyleBackColor = true;
            this.lefoff.Click += new System.EventHandler(this.ledoff_Click);
            // 
            // servoUP
            // 
            this.servoUP.Location = new System.Drawing.Point(69, 9);
            this.servoUP.Name = "servoUP";
            this.servoUP.Size = new System.Drawing.Size(55, 23);
            this.servoUP.TabIndex = 9;
            this.servoUP.Text = "D8+";
            this.servoUP.UseVisualStyleBackColor = true;
            this.servoUP.Click += new System.EventHandler(this.servoUP_Click);
            // 
            // servoCNT
            // 
            this.servoCNT.Location = new System.Drawing.Point(69, 38);
            this.servoCNT.Name = "servoCNT";
            this.servoCNT.Size = new System.Drawing.Size(55, 23);
            this.servoCNT.TabIndex = 10;
            this.servoCNT.Text = "CNT";
            this.servoCNT.UseVisualStyleBackColor = true;
            this.servoCNT.Click += new System.EventHandler(this.servoCNT_Click);
            // 
            // servoDN
            // 
            this.servoDN.Location = new System.Drawing.Point(69, 67);
            this.servoDN.Name = "servoDN";
            this.servoDN.Size = new System.Drawing.Size(55, 23);
            this.servoDN.TabIndex = 11;
            this.servoDN.Text = "D8-";
            this.servoDN.UseVisualStyleBackColor = true;
            this.servoDN.Click += new System.EventHandler(this.servoDOWN_Click);
            // 
            // servoLEFT
            // 
            this.servoLEFT.Location = new System.Drawing.Point(13, 38);
            this.servoLEFT.Name = "servoLEFT";
            this.servoLEFT.Size = new System.Drawing.Size(55, 23);
            this.servoLEFT.TabIndex = 12;
            this.servoLEFT.Text = "D9-,10+";
            this.servoLEFT.UseVisualStyleBackColor = true;
            this.servoLEFT.Click += new System.EventHandler(this.servoLEFT_Click);
            // 
            // servoRIGHT
            // 
            this.servoRIGHT.Location = new System.Drawing.Point(125, 38);
            this.servoRIGHT.Name = "servoRIGHT";
            this.servoRIGHT.Size = new System.Drawing.Size(55, 23);
            this.servoRIGHT.TabIndex = 13;
            this.servoRIGHT.Text = "D9+,10-";
            this.servoRIGHT.UseVisualStyleBackColor = true;
            this.servoRIGHT.Click += new System.EventHandler(this.servoRIGHT_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(0, 329);
            this.trackBar1.Maximum = 255;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(477, 45);
            this.trackBar1.TabIndex = 1;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 316);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(454, 11);
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(12, 369);
            this.textBox3.MaxLength = 16;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(218, 21);
            this.textBox3.TabIndex = 24;
            this.textBox3.Text = "Hello XBOT";
            // 
            // printLCD1
            // 
            this.printLCD1.Location = new System.Drawing.Point(236, 369);
            this.printLCD1.Name = "printLCD1";
            this.printLCD1.Size = new System.Drawing.Size(54, 23);
            this.printLCD1.TabIndex = 25;
            this.printLCD1.Text = "LCD_1";
            this.printLCD1.UseVisualStyleBackColor = true;
            this.printLCD1.Click += new System.EventHandler(this.printLCD1_Click);
            // 
            // printLCD2
            // 
            this.printLCD2.Location = new System.Drawing.Point(296, 369);
            this.printLCD2.Name = "printLCD2";
            this.printLCD2.Size = new System.Drawing.Size(52, 23);
            this.printLCD2.TabIndex = 26;
            this.printLCD2.Text = "LCD_2";
            this.printLCD2.UseVisualStyleBackColor = true;
            this.printLCD2.Click += new System.EventHandler(this.printLCD2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.servoRIGHT);
            this.groupBox1.Controls.Add(this.servoLEFT);
            this.groupBox1.Controls.Add(this.servoDN);
            this.groupBox1.Controls.Add(this.servoCNT);
            this.groupBox1.Controls.Add(this.servoUP);
            this.groupBox1.Location = new System.Drawing.Point(260, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(197, 95);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Servo";
            // 
            // motorRIGHT
            // 
            this.motorRIGHT.Location = new System.Drawing.Point(123, 39);
            this.motorRIGHT.Name = "motorRIGHT";
            this.motorRIGHT.Size = new System.Drawing.Size(55, 23);
            this.motorRIGHT.TabIndex = 32;
            this.motorRIGHT.Text = "RIGHT";
            this.motorRIGHT.UseVisualStyleBackColor = true;
            this.motorRIGHT.Click += new System.EventHandler(this.motorRIGHT_Click);
            // 
            // motorLEFT
            // 
            this.motorLEFT.Location = new System.Drawing.Point(11, 39);
            this.motorLEFT.Name = "motorLEFT";
            this.motorLEFT.Size = new System.Drawing.Size(55, 23);
            this.motorLEFT.TabIndex = 31;
            this.motorLEFT.Text = "LEFT";
            this.motorLEFT.UseVisualStyleBackColor = true;
            this.motorLEFT.Click += new System.EventHandler(this.motorLEFT_Click);
            // 
            // motorBWD
            // 
            this.motorBWD.Location = new System.Drawing.Point(67, 68);
            this.motorBWD.Name = "motorBWD";
            this.motorBWD.Size = new System.Drawing.Size(55, 23);
            this.motorBWD.TabIndex = 30;
            this.motorBWD.Text = "BWD";
            this.motorBWD.UseVisualStyleBackColor = true;
            this.motorBWD.Click += new System.EventHandler(this.motorBWD_Click);
            // 
            // motorSTP
            // 
            this.motorSTP.Location = new System.Drawing.Point(67, 39);
            this.motorSTP.Name = "motorSTP";
            this.motorSTP.Size = new System.Drawing.Size(55, 23);
            this.motorSTP.TabIndex = 29;
            this.motorSTP.Text = "STOP";
            this.motorSTP.UseVisualStyleBackColor = true;
            this.motorSTP.Click += new System.EventHandler(this.motorSTP_Click);
            // 
            // motorFWD
            // 
            this.motorFWD.Location = new System.Drawing.Point(67, 10);
            this.motorFWD.Name = "motorFWD";
            this.motorFWD.Size = new System.Drawing.Size(55, 23);
            this.motorFWD.TabIndex = 28;
            this.motorFWD.Text = "FWD";
            this.motorFWD.UseVisualStyleBackColor = true;
            this.motorFWD.Click += new System.EventHandler(this.motorFWD_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.motorRIGHT);
            this.groupBox2.Controls.Add(this.motorLEFT);
            this.groupBox2.Controls.Add(this.motorBWD);
            this.groupBox2.Controls.Add(this.motorSTP);
            this.groupBox2.Controls.Add(this.motorFWD);
            this.groupBox2.Location = new System.Drawing.Point(261, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(193, 95);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wheel";
            // 
            // Tone
            // 
            this.Tone.Location = new System.Drawing.Point(356, 369);
            this.Tone.Name = "Tone";
            this.Tone.Size = new System.Drawing.Size(54, 23);
            this.Tone.TabIndex = 34;
            this.Tone.Text = "Tone";
            this.Tone.UseVisualStyleBackColor = true;
            this.Tone.Click += new System.EventHandler(this.Tone_Click);
            // 
            // oled
            // 
            this.oled.Location = new System.Drawing.Point(177, 235);
            this.oled.Name = "oled";
            this.oled.Size = new System.Drawing.Size(75, 23);
            this.oled.TabIndex = 35;
            this.oled.Text = "OLED IMG";
            this.oled.UseVisualStyleBackColor = true;
            this.oled.Click += new System.EventHandler(this.oled_Click);
            // 
            // BTRemote
            // 
            this.BTRemote.Location = new System.Drawing.Point(415, 369);
            this.BTRemote.Name = "BTRemote";
            this.BTRemote.Size = new System.Drawing.Size(54, 23);
            this.BTRemote.TabIndex = 36;
            this.BTRemote.Text = "BTR";
            this.BTRemote.UseVisualStyleBackColor = true;
            this.BTRemote.Click += new System.EventHandler(this.BTRemote_Click);
            // 
            // H_TRIM
            // 
            this.H_TRIM.Location = new System.Drawing.Point(415, 10);
            this.H_TRIM.Name = "H_TRIM";
            this.H_TRIM.Size = new System.Drawing.Size(62, 23);
            this.H_TRIM.TabIndex = 37;
            this.H_TRIM.Text = "H_TRIM";
            this.H_TRIM.UseVisualStyleBackColor = true;
            this.H_TRIM.Click += new System.EventHandler(this.HTRIM_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 400);
            this.Controls.Add(this.H_TRIM);
            this.Controls.Add(this.BTRemote);
            this.Controls.Add(this.oled);
            this.Controls.Add(this.Tone);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.printLCD2);
            this.Controls.Add(this.printLCD1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.lefoff);
            this.Controls.Add(this.ledon);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "BingleS C#";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button ledon;
        private System.Windows.Forms.Button lefoff;
        private System.Windows.Forms.Button servoUP;
        private System.Windows.Forms.Button servoCNT;
        private System.Windows.Forms.Button servoDN;
        private System.Windows.Forms.Button servoLEFT;
        private System.Windows.Forms.Button servoRIGHT;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button printLCD1;
        private System.Windows.Forms.Button printLCD2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button motorRIGHT;
        private System.Windows.Forms.Button motorLEFT;
        private System.Windows.Forms.Button motorBWD;
        private System.Windows.Forms.Button motorSTP;
        private System.Windows.Forms.Button motorFWD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Tone;
        private System.Windows.Forms.Button oled;
        private System.Windows.Forms.Button BTRemote;
        private System.Windows.Forms.Button H_TRIM;


    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

using Microsoft.Win32;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Management;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;

namespace EPOR
{
    public partial class Form1 : Form
    {
        const int MaxBuffer = 5000;
        byte[] buff = new byte[MaxBuffer];

        const int MaxFIFO = 100000;
        byte[] m_pBuf = new byte[MaxFIFO];
        //byte[] PictureData = new byte[MaxFIFO];

        int frameCount = 0;
        int m_receiveMode = 0;
        int addr = 0;
        int ImageSize = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            comboBox1.Items.Clear();

            ManagementObjectSearcher comPortSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0");

            string caption = null;
            foreach (ManagementObject obj in comPortSearcher.Get())
            {
                if (obj != null)
                {
                    object captionObj = obj["Caption"];

                    if (captionObj != null)
                    {
                        caption = captionObj.ToString();

                        if (caption.Contains("(COM"))//&& (caption.Contains("Bluetooth") || caption.Contains("BT")))
                        {
                            string Name = caption.Substring(caption.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")", string.Empty);
                            Console.WriteLine(Name + "," + caption);

                            comboBox1.Items.Add(Name);
                        }
                    }
                }
            }

            comboBox1.SelectedIndex = comboBox1.Items.Count - 1;


            //serialPort1.ReadTimeout = 1;
            //serialPort1.WriteTimeout = 300;

            serialPort1.BaudRate = 115200;
            serialPort1.Encoding = Encoding.Default;
            serialPort1.Parity = Parity.None;
            serialPort1.DataBits = 8;
            serialPort1.StopBits = StopBits.One;
            serialPort1.Handshake = Handshake.None;


            timer1.Enabled = true;
        }

        public int ByteIndexOf(byte[] searched, byte[] find, int start, int end)
        {
            // Do standard error checking here.
            bool matched = false;

            for (int index = start; index <= end - find.Length; ++index)
            {
                // Assume the values matched.
                matched = true;

                // Search in the values to be found.
                for (int subIndex = 0; subIndex < find.Length; ++subIndex)
                {
                    // Check the value in the searched array vs the value
                    // in the find array.
                    if (find[subIndex] != searched[index + subIndex])
                    {
                        // The values did not match.
                        matched = false;

                        // Break out of the loop.
                        break;
                    }
                }

                // If the values matched, return the index.
                if (matched)
                {
                    // Return the index.
                    return index;
                }
            }

            // None of the values matched, return -1.
            return -1;
        }

        byte[] header = new byte[3] { (byte)'R', (byte)'X', (byte)'=' };
        byte[] adcValue = new byte[6];
        byte[] RxValue = new byte[3];

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            //Console.WriteLine("Got from " + e.ToString());
            try
            {
                int fSize = 0;

                if (m_receiveMode == 0 && serialPort1.BytesToRead >= 15)
                {
                    fSize = serialPort1.Read(buff, 0, MaxBuffer);

                    int index = ByteIndexOf(buff, header, 0, fSize);
                    if (index != -1)
                    {
                        ImageSize = 0;

                        if (buff[index + 3] == '1' && buff[index + 4]=='0')
                        {
                            ImageSize = 10;
                        }
                        else
                        {
                            ImageSize = (int)(buff[index + 3] & 0x00ff) << 8;
                            ImageSize = ImageSize | (buff[index + 4] & 0xff);
                        }


                        //Console.WriteLine("Got header data ! {0} {1} {2} , size {3} ", (char)buff[index], (char)buff[index + 1], (char)buff[index + 2],ImageSize);

                        int imageBase = index + 5;
                        if (ImageSize == 10 && buff[imageBase + 9] == 0x30)
                        {
                            adcValue[0] = buff[imageBase];
                            adcValue[1] = buff[imageBase + 1];
                            adcValue[2] = buff[imageBase + 2];
                            adcValue[3] = buff[imageBase + 3];
                            adcValue[4] = buff[imageBase + 4];
                            adcValue[5] = buff[imageBase + 5];

                            RxValue[0] = buff[imageBase + 6];
                            RxValue[1] = buff[imageBase + 7];
                            RxValue[2] = buff[imageBase + 8];
                            
                            this.Invoke(new EventHandler(ShowChartData));
                        }
                        else
                        {
                            addr = 0;
                            //Array.Clear(m_pBuf, 0, m_pBuf.Length);
                            Buffer.BlockCopy(buff, imageBase, m_pBuf, addr, fSize - imageBase);
                            addr += (fSize - imageBase);

                            m_receiveMode = 1;

                            Console.WriteLine("Rcv header size: " + ImageSize);
                        }
                    }
                }
                if (m_receiveMode == 1)
                {
                    if (addr < ImageSize)
                    {
                        fSize = serialPort1.Read(buff, 0, MaxBuffer);
                        Buffer.BlockCopy(buff, 0, m_pBuf, addr, fSize);
                        addr += fSize;
                    }

                    if (addr >= ImageSize)
                    {
                        Console.WriteLine("Size {0} {1}", addr, ImageSize);

                        frameCount++;
                        m_receiveMode = 0;

                        for (int i = 0; i < 5; i++)
                            adcValue[i] = m_pBuf[i];

                        adcValue[5] = m_pBuf[ImageSize - 5];
                        Console.WriteLine("Got adc data ! {0} {1} {2} {3} {4} {5} ", adcValue[0], adcValue[1], adcValue[2], adcValue[3], adcValue[4], adcValue[5]);

                        RxValue[0] = m_pBuf[ImageSize - 4];
                        RxValue[1] = m_pBuf[ImageSize - 3];
                        RxValue[2] = m_pBuf[ImageSize - 2];
                        Console.WriteLine("Got RX serial data !  {0:x} {1:x} {2:x}", RxValue[0], RxValue[1], RxValue[2]);// Rx data

                        //Console.WriteLine("Got tail !  {0} {1}", m_pBuf[ImageSize - 1], (int)'0');// End byte is always 0x30 == 48

                        if (ImageSize > 50)
                            this.Invoke(new EventHandler(ShowJpegData));
                        else
                            this.Invoke(new EventHandler(ShowChartData));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("serialPort_DataReceived Exception = " + ex.ToString());
            }
        }

        public Bitmap ResizeBitmap(Bitmap b, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            return result;
        }


        private void DrawDots(Graphics g, Point p1)
        {
            Pen pen = new Pen(Color.SeaGreen);
            g.DrawPie(pen, p1.X - 5, p1.Y - 5, 10, 10, 0, 360);
            g.FillPie(new SolidBrush(Color.Purple), p1.X - 5, p1.Y - 5, 10, 10, 0, 360);
        }

        private void ShowChartData(object sender, EventArgs e)
        {
            try
            {
                int[] alWeight = new int[] { 255, 23, 33, 15, 200, 10 };

                int topX = 0;
                int topY = 0;

                int lengthArea = 240;
                int heightArea = 180;

                //int numberOfSections = alWeight.Length;
                int numberOfSections = 6;
                int maxWeight = 255;

                int[] height = new int[numberOfSections];
                //Random rnd = new Random();

                Color[] brushCol = new Color[] { Color.CadetBlue, Color.Cyan, Color.Blue, Color.DarkRed, Color.Coral, Color.Green };
                Graphics g = Graphics.FromImage(pictureBox1.Image);

                g.Clear(Color.White);
                SolidBrush brush = new SolidBrush(Color.Aquamarine);
                Pen pen = new Pen(Color.Gray);

                Rectangle rec = new Rectangle(topX, topY, lengthArea - 1, heightArea);
                g.DrawRectangle(pen, rec);

                pen.Color = Color.Black;

                int smallX = topX;
                int smallY = 0;
                int smallLength = (lengthArea / alWeight.Length);
                int smallHeight = 0;

                for (int i = 0; i < numberOfSections; i++)
                {
                    alWeight[i] = (int)adcValue[i];
                    //brush.Color = Color.FromArgb(rnd.Next(200, 255), rnd.Next(255), rnd.Next(255), rnd.Next(255));
                    brush.Color = brushCol[i];

                    smallHeight = ((alWeight[i] * heightArea) / maxWeight);
                    smallY = topY + heightArea - smallHeight;
                    Rectangle rectangle = new Rectangle(smallX, smallY, smallLength, smallHeight);
                    g.DrawRectangle(pen, rectangle);
                    g.FillRectangle(brush, rectangle);
                    brush.Color = Color.FromKnownColor(KnownColor.Black);
                    g.DrawRectangle(pen, rectangle);
                    smallX = smallX + smallLength;
                }
                pictureBox1.Invalidate();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ShowChartData Exception = " + ex.ToString());
            }
        }

        private void ShowJpegData(object sender, EventArgs e)
        {
            try
            {
                byte[] PictureData = new byte[ImageSize - 10];
                Buffer.BlockCopy(m_pBuf, 5, PictureData, 0, ImageSize - 10);

                System.IO.MemoryStream memstr = new System.IO.MemoryStream(PictureData);
                memstr.Seek(0, SeekOrigin.Begin);

                //pictureBox1.Image = new Bitmap(memstr);

                Bitmap currImage = ResizeBitmap(new Bitmap(memstr), 240, 180);
                pictureBox1.Image = currImage;
            }
            catch (Exception ex)
            {
                Console.WriteLine("ShowJpegData Exception = " + ex.ToString());
            }
        }

        override protected void OnClosing(CancelEventArgs a_rE)
        {
            SafeDisconnect();

            //MessageBox.Show("OnClosing", "Error");
            //base.OnClosing(a_rE);
            Application.Exit();
        }

        public void SafeDisconnect()
        {
            if (serialPort1.IsOpen)
            {
                GC.SuppressFinalize(serialPort1.BaseStream);
                try { serialPort1.Close(); }
                catch { }
                serialPort1 = null;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SafeDisconnect();
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Select Comport", "Error");
            }
            else if (serialPort1.IsOpen)
            {
                serialPort1.DataReceived -= new SerialDataReceivedEventHandler(serialPort_DataReceived);

                serialPort1.Close();
                label1.Text = "Ready";
                button1.Text = "Connect";


                comboBox1.Items.Clear();

                ManagementObjectSearcher comPortSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0");

                string caption = null;
                foreach (ManagementObject obj in comPortSearcher.Get())
                {
                    if (obj != null)
                    {
                        object captionObj = obj["Caption"];

                        if (captionObj != null)
                        {
                            caption = captionObj.ToString();

                            if (caption.Contains("(COM"))//&& (caption.Contains("Bluetooth") || caption.Contains("BT")))
                            {
                                string Name = caption.Substring(caption.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")", string.Empty);
                                Console.WriteLine(Name + "," + caption);

                                comboBox1.Items.Add(Name);
                            }
                        }
                    }
                }

                comboBox1.SelectedIndex = comboBox1.Items.Count - 1;

            }
            else if (!serialPort1.IsOpen)
            {
                try
                {
                    //MessageBox.Show(serialPort1.PortName);
                    serialPort1.PortName = comboBox1.Text;
                    serialPort1.Open();
                    serialPort1.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);

                    if (serialPort1.IsOpen)
                    {
                        label1.Text = "Opened";
                        button1.Text = "Disconnect";
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("serialPort Exception = " + ex.ToString());
                }

            }
        }

        public struct LCDCMD
        {
            public bool isSet;
            public int lcdLine;
            public string[] lcdText;

            public LCDCMD(int nstr)
            {
                isSet = false;
                lcdLine = 0;
                lcdText = new string[nstr];
                lcdText[0] = "                ";
                lcdText[1] = "                ";
            }
        }

        LCDCMD lcdCmd = new LCDCMD(2);


        private void printLCD1_Click(object sender, EventArgs e)
        {
            SetLcdStr(0, textBox3.Text);
        }

        private void printLCD2_Click(object sender, EventArgs e)
        {
            SetLcdStr(1, textBox3.Text);
        }


        int MotorRight = 0, MotorLeft = 0;
        int ServoHead = 90, ServoArm1 = 90, ServoArm2 = 90;
        int LedR = 0, LedG = 0, LedB = 0;

        public static int MAX_CMD = 9;

        const char MOTOR_SPEED  = '0';
        const char MOTOR_GO     = '1';
        const char SERVO_ANGLE  = '2';
        const char RGB_WRITE    = '3';
        const char TONE_PLAY    = '4';
        const char HEAD_LED     = '5';
        const char BT_REMOCON   = '6';
        const char LCD_TEXT     = '7';
        const char OLED_IMAGE   = '8';
        const char HEAD_TRIM    = '9';


        int btRemoteCode = 1;
        private void BTRemote_Click(object sender, EventArgs e)
        {
            String btCode = btRemoteCode.ToString("000");

            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[5];

                tmpBuffer[0] = (byte)BT_REMOCON;
                tmpBuffer[1] = (byte)btCode[0];
                tmpBuffer[2] = (byte)btCode[1];
                tmpBuffer[3] = (byte)btCode[2];
                tmpBuffer[4] = (byte)'S';

                serialPort1.Write(tmpBuffer, 0, 5);
                Thread.Sleep(20);
            }
        }

        int imageNum = 0;
        private void oled_Click(object sender, EventArgs e)
        {
            String imgNum = imageNum.ToString("000");

            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[5];

                tmpBuffer[0] = (byte)OLED_IMAGE;
                tmpBuffer[1] = (byte)imgNum[0];
                tmpBuffer[2] = (byte)imgNum[1];
                tmpBuffer[3] = (byte)imgNum[2];
                tmpBuffer[4] = (byte)'S';

                serialPort1.Write(tmpBuffer, 0, 5);
                Thread.Sleep(20);
            }

            imageNum++;
            if (imageNum > 30) imageNum = 0;
        }

        void SetLcdStr(int lcdLine, string lcdtext)
        {
            if (lcdLine < 0) lcdLine = 0;
            if (lcdLine > 1) lcdLine = 1;

            if (!lcdCmd.lcdText[lcdLine].Equals(lcdtext))
            {
                lcdCmd.lcdLine = lcdLine;
                lcdCmd.lcdText[lcdLine] = lcdtext;
                lcdCmd.isSet = true;
            }

            if (lcdCmd.isSet)
            {
                if (lcdCmd.lcdText[lcdCmd.lcdLine].Length > 0)
                {
                    string tmpStr = lcdCmd.lcdText[lcdCmd.lcdLine] + "                                               ";
                    char[] sendText = tmpStr.ToCharArray();

                    if (serialPort1.IsOpen)
                    {
                        byte[] tmpBuffer = new byte[13];

                        tmpBuffer[0] = (byte)LCD_TEXT;
                        tmpBuffer[1] = (byte)(lcdLine+0x30);

                        tmpBuffer[2] = (byte)sendText[0];
                        tmpBuffer[3] = (byte)sendText[1];
                        tmpBuffer[4] = (byte)sendText[2];
                        tmpBuffer[5] = (byte)sendText[3];
                        tmpBuffer[6] = (byte)sendText[4];
                        tmpBuffer[7] = (byte)sendText[5];
                        tmpBuffer[8] = (byte)sendText[6];
                        tmpBuffer[9] = (byte)sendText[7];
                        tmpBuffer[10] = (byte)sendText[8];
                        tmpBuffer[11] = (byte)sendText[9];
                        
                        
                        tmpBuffer[12] = (byte)'S';

                        serialPort1.Write(tmpBuffer, 0, 13);
                        Thread.Sleep(20);
                    }
                }
            }
        }

        void SetRGBFromHue(int H) // H is Interval [0, 255]
        {
            if (H > 255) H = 255;
            
            if (H <= 0)
            {
                H = 0;
                LedR = LedG = LedB = 0;
            }
            else if (H < 85)
            {
                LedR = 255 - H * 3;
                LedG = H * 3;
                LedB = 0;
            }
            else if (H < 170)
            {
                LedR = 0;
                LedG = 510 - H * 3;
                LedB = H * 3 - 255;
            }
            else
            {
                LedR = H * 3 - 510;
                LedG = 0;
                LedB = (255 - H) * 3;
            }

            LedR = (int)((float)LedR * 0.5);
            LedG = (int)((float)LedG * 0.5);
            LedB = (int)((float)LedB * 0.5);

            if (LedR > 255) LedR = 255;
            if (LedG > 255) LedG = 255;
            if (LedB > 255) LedB = 255;

            //Console.WriteLine("{0} {1} {2} ", R, G, B);
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            textBox2.Text += "\r\nRGB index = " + trackBar1.Value.ToString();
            SetRGBFromHue(trackBar1.Value);
            
            
            //SendXBOTcmdReal(RGB_WRITE, 255, (byte)(LedR), (byte)(LedG), (byte)(LedB), 0);

            string red, green, blue;

            red = LedR.ToString("000");
            green = LedG.ToString("000");
            blue = LedB.ToString("000");

            Console.WriteLine("red {0} green {1} blue {2}", red, green, blue);


            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[12];

                tmpBuffer[0] = (byte)RGB_WRITE;
                tmpBuffer[1] = (byte)'0';
                tmpBuffer[2] = (byte)red[0];
                tmpBuffer[3] = (byte)red[1];
                tmpBuffer[4] = (byte)red[2];
                tmpBuffer[5] = (byte)green[0];
                tmpBuffer[6] = (byte)green[1];
                tmpBuffer[7] = (byte)green[2];
                tmpBuffer[8] = (byte)blue[0];
                tmpBuffer[9] = (byte)blue[1];
                tmpBuffer[10] = (byte)blue[2];
                tmpBuffer[11] = (byte)'S';


                serialPort1.Write(tmpBuffer, 0, 12);
                Thread.Sleep(20);
            }
        }

        void PlayTone(string tone, int duration)
        {
            if (duration < 0) duration = 0;
            else if (duration > 0) duration /= 100;

            if (duration > 99) duration = 99;

            string dur = duration.ToString("00");

            Console.WriteLine("dur {0}", dur);


            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[6];

                tmpBuffer[0] = (byte)TONE_PLAY;
                tmpBuffer[1] = (byte)tone[0];
                tmpBuffer[2] = (byte)tone[1];
                tmpBuffer[3] = (byte)dur[0];
                tmpBuffer[4] = (byte)dur[1];
                tmpBuffer[5] = (byte)'S';


                serialPort1.Write(tmpBuffer, 0, 6);
                Thread.Sleep(20);
            }

        }

        private void ledon_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[3];

                tmpBuffer[0] = (byte)HEAD_LED;
                tmpBuffer[1] = (byte)'1';
                tmpBuffer[2] = (byte)'S';

                serialPort1.Write(tmpBuffer, 0, 3);

                Thread.Sleep(20);
            }
        }

        private void ledoff_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[3];

                tmpBuffer[0] = (byte)HEAD_LED;
                tmpBuffer[1] = (byte)'0';
                tmpBuffer[2] = (byte)'S';

                serialPort1.Write(tmpBuffer, 0, 3);

                Thread.Sleep(20);
            }

        }

        void setServoAngle(int angle1, int angle2, int angle3)
        {
            if (angle1 < 10) angle1 = 10;
            else if (angle1 > 170) angle1 = 170;

            if (angle2 < 10) angle2 = 10;
            else if (angle2 > 170) angle2 = 170;

            if (angle3 < 10) angle3 = 10;
            else if (angle3 > 170) angle3 = 170;

            string head, left, right;

            head = angle1.ToString("000");
            left = angle2.ToString("000");
            right = angle3.ToString("000");

            Console.WriteLine("head {0} left {1} right {2}", head, left, right);


            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[11];

                tmpBuffer[0] = (byte)SERVO_ANGLE;
                tmpBuffer[1] = (byte)head[0];
                tmpBuffer[2] = (byte)head[1];
                tmpBuffer[3] = (byte)head[2];
                tmpBuffer[4] = (byte)left[0];
                tmpBuffer[5] = (byte)left[1];
                tmpBuffer[6] = (byte)left[2];
                tmpBuffer[7] = (byte)right[0];
                tmpBuffer[8] = (byte)right[1];
                tmpBuffer[9] = (byte)right[2];
                tmpBuffer[10] = (byte)'S';


                serialPort1.Write(tmpBuffer, 0, 11);
                Thread.Sleep(20);
            }
        }


        private void servoUP_Click(object sender, EventArgs e)
        {
            ServoHead += 20;
            setServoAngle(ServoHead, ServoArm1, ServoArm2);
        }

        private void servoDOWN_Click(object sender, EventArgs e)
        {
            ServoHead -= 20;
            setServoAngle(ServoHead, ServoArm1, ServoArm2);
        }

        private void servoCNT_Click(object sender, EventArgs e)
        {
            ServoHead = ServoArm1 = ServoArm2 = 90;
            setServoAngle(ServoHead, ServoArm1, ServoArm2);
        }

        private void servoLEFT_Click(object sender, EventArgs e)
        {
            ServoArm1 -= 20;
            ServoArm2 += 20;
            setServoAngle(ServoHead, ServoArm1, ServoArm2);
        }

        private void servoRIGHT_Click(object sender, EventArgs e)
        {
            ServoArm1 += 20;
            ServoArm2 -= 20;
            setServoAngle(ServoHead, ServoArm1, ServoArm2);
        }

        void setMotorSpeed(int speed1, int speed2)
        {
            if (speed1 > 255) speed1 = 255;
            else if (speed1 < -255) speed1 = -255;
            if (speed2 > 255) speed2 = 255;
            else if (speed2 < -255) speed2 = -255;

            string left, right;

            if(speed1<0)
                right = speed1.ToString("000");
            else
                right = speed1.ToString("+000");

            if (speed2 < 0)
                left = speed2.ToString("000");
            else
                left = speed2.ToString("+000");

            Console.WriteLine("left {0} right {1}", left, right);


            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[10];

                tmpBuffer[0] = (byte)MOTOR_SPEED;
                tmpBuffer[1] = (byte)left[0];
                tmpBuffer[2] = (byte)left[1];
                tmpBuffer[3] = (byte)left[2];
                tmpBuffer[4] = (byte)left[3];
                tmpBuffer[5] = (byte)right[0];
                tmpBuffer[6] = (byte)right[1];
                tmpBuffer[7] = (byte)right[2];
                tmpBuffer[8] = (byte)right[3];
                tmpBuffer[9] = (byte)'S';


                serialPort1.Write(tmpBuffer, 0, 10);
                Thread.Sleep(20);
            }
        }

        public void sendMotor(int right, int left)
        {

            if (MotorRight != (int)right || MotorLeft != (int)left)
            {
                MotorRight = (int)right;
                MotorLeft = (int)left;

                setMotorSpeed(MotorRight, MotorLeft);
            }
        }
        private void motorFWD_Click(object sender, EventArgs e)
        {
            sendMotor(170, 170);
        }

        private void motorSTP_Click(object sender, EventArgs e)
        {
            sendMotor(0, 0);
        }

        private void motorBWD_Click(object sender, EventArgs e)
        {
            sendMotor(-170, -170);
        }

        private void motorLEFT_Click(object sender, EventArgs e)
        {
            sendMotor(170, -170);
        }

        private void motorRIGHT_Click(object sender, EventArgs e)
        {
            sendMotor(-170, 170);
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            //textBox1.Text = frameCount.ToString();
            frameCount = 0;
            textBox2.Text = string.Format("ADC Data [ {0} {1} {2} {3} {4} {5} ] | Remocon [ {6} ] Battery [ {7} ]", adcValue[0], adcValue[1], adcValue[2], adcValue[3], adcValue[4], adcValue[5], RxValue[0], RxValue[1]);

        }

        private void Tone_Click(object sender, EventArgs e)
        {
            PlayTone("C4", 100);
        }

        public bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream =
                   new System.IO.FileStream(_FileName, System.IO.FileMode.Create,
                                            System.IO.FileAccess.Write);
                // Writes a block of bytes to this stream using data from a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);

                // close file stream
                _FileStream.Close();

                return true;
            }
            catch (Exception ex)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}",
                                  ex.ToString());
            }

            // error occured, return false
            return false;
        }

        
        byte camOnOff = 1;
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            camOnOff ^= 1;
            Console.WriteLine("pictureBox1_Click! {0} ", camOnOff);
        }

        private void HTRIM_Click(object sender, EventArgs e)
        {
            int headTrimAng = Convert.ToInt32(textBox1.Text)%90;
            String trim = headTrimAng.ToString("000");

            if (serialPort1.IsOpen)
            {
                byte[] tmpBuffer = new byte[5];

                tmpBuffer[0] = (byte)HEAD_TRIM;
                tmpBuffer[1] = (byte)trim[0];
                tmpBuffer[2] = (byte)trim[1];
                tmpBuffer[3] = (byte)trim[2];
                tmpBuffer[4] = (byte)'S';

                serialPort1.Write(tmpBuffer, 0, 5);
                Thread.Sleep(20);
            }
        }

    }
}